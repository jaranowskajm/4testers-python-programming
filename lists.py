# Escaping characters apostrofy i cudzysłów
restaurant = ['KFC', 'Burger King', 'McDonald\'s']
print('McDonals\'s')

print("My favourite book is \"Baśń o wężowym sercu albo wtóre słowo o Jakóbie Szeli\"")

# Lists exercises



movies = ["\"Pulp fiction\"", "\"Marcel the shell\"", "\"Django\"", "\"Eternal sunshine of the spotless mind\"",
          "\"Witch\""]
first_movie = movies[0]
# bez f stringów
print("First movie is", first_movie)

# z f stringiem
print(f'First movie is {first_movie}.')

last_movie = movies[-1]
print(f'Last movie is {last_movie}.')

# można też tak:
print(f"Last movie is {movies[-1]}.")

movies.append("\"Orzeł kontra rekin\"")
print(f"The lenght of movies list is {len(movies)}.")

print(f'The lenght of word supercalifragilistc is {len("supercalifragilistc")}')

# dodawanie i usuwanie elementów listy
movies.insert(0, "\"Star Wars\"")
movies.remove("\"Marcel the shell\"")

print(movies)

# zmiana miejscami 2 elementów - wywołać przez index i wpisać na co zmieniamy
movies[-1] = "Dune (1980)"
print(movies)

# Task 1
emails = ['a@example.com', 'b@example.com']

print(f'The lenght of emails list is {len(emails)}')

print(f'First email is {emails[0]}')

print(f'Last email is {emails[-1]}')

emails.append('cde@example.com')

print(emails)

print(f'The lenght of emails list is {len(emails)}')

# or
emails = ['a@example.com', 'b@example.com']
print(len(emails))
print(emails[0])
print(emails[-1])

emails.append('cde@example.com')


