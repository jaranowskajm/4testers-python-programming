my_name = "Julia"
my_age = 27
my_email = "jullia.jaranowska@gmail.com"

print(my_name)
print(my_age)
print(my_email)

# The description of my friend

friend_name = "Kasia"
friend_age = "28"
number_of_pets = "2"
friend_has_driving_license = True
years_of_friendship_in_years = "6.5"

print(friend_name, friend_age, number_of_pets, friend_has_driving_license, years_of_friendship_in_years, sep="\t")

