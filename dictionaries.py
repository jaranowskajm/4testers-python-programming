# Dictionaries exercises

student = {
    'name': 'Adam Iksiński',
    'grades': [3, 4, 5, 5.5],
    'subjects': ['test', 'IT']
}

# Task 1

friend = {
    'name': 'Kasia',
    'age': 34.5,
    'hobbies': ['roller skating', 'swimming']
}