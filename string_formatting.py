my_name = "Julia"
favourite_movie = "Marcel the Shell with Shoes On"
favourite_actor = "Uma Thurman"

print("My name is ", my_name, ". My favourite movie is ", favourite_movie, ". My favourite actor is ", favourite_actor,
      ".", sep="")

bio = f"My name is {my_name}. My favourite movie is {favourite_movie}. My favourite actor is {favourite_actor}."
print(bio)

calculation_example = f"Sum od 2 and 4 is {2 + 4}."
print(calculation_example)

# Exercise 1 - Print welcome message to a person in a city
name_1 = "Michał"
name_2 = "Beata"
city_1 = "Toruń"
city_2 = "Gdynia"

welcome_inscription_1 = f"Witaj {name_1}! Miło Cię widzieć w naszym mieście: {city_1}!"
welcome_inscription_2 = f"Witaj {name_2}! Miło Cię widzieć w naszym mieście: {city_2}!"
print(welcome_inscription_1)
print(welcome_inscription_2)

def print_hello_message(name,city):
      name_capitalized = name.capitalize()
      city_capitalazed = city.capitalize()
      print(f"Witaj {name_capitalized}! Miło Cię widzieć w naszym mieście: {city_capitalazed}!")

print_hello_message("Michał", "Toruń")
print_hello_message("Beata", "Gdynia")
print_hello_message("adam", "kraków")

# Exercise 2 - Generate email in 4testers.pl domain using first and last name
def get_email(name, surname):
      return f'{name.lower()}.{surname.lower()}@4testers.pl'

print(get_email("Janusz", "Nowak"))
print(get_email("Barbara", "Kowalska"))


# Example of how not to do it - to much work with changes
employee_1_first_name = "Barbara"
employee_1_last_name = "Nowak"
employee_1_email =f"{employee_1_first_name.lower()}.{employee_1_last_name.lower()}@4_testers.pl"
print(employee_1_email)